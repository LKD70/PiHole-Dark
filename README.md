# PiHole-Dark
A Dark theme for the pihole admin page. (Now FTL compatible)



----------

## Easy install

1. open a terminal (or SSH if you're remoting in) ( <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>T</kbd> )
2. Type: `cd /var/www/html/`
3. Type: `sudo wget https://raw.githubusercontent.com/lkd70/PiHole-Dark/master/install.sh`
4. Type: `sudo chmod +x install.sh`
5. Type: `sudo ./install.sh`
6. Follow the onscreen instructions!

Once you've done the install, here's a few recommended steps:

** Disable (uncheck) 'Use boxed layout' option to enable fullscreen **
1. Open the pi-hole admin web interface.
2. Navigate to the settings section, the to the API/Web Interface tab.
3. Under 'Interface appearance' uncheck the 'Use boxed layout' option.

If you'd like to keep the boxed layout, i recommend you upload a custom image in place of the defauult background image:

**Adding a custom background image**
1. FInd an image to use as your new background. Maybe something from a site like: https://www.toptal.com/designers/subtlepatterns/
(Keep in mind, by default the image is repeated, though this can be changed in the `AdminLTE.min.css` file)
2. Rename the image to `boxed-bg.jpg`
3. Place the image in the `/var/www/html/admin/img/` directory on your pi - replacing any existing file.
4. Reload your pi web admin page.

----------

## Easy Uninstall

Not a fan of the style? Sorry to hear, please feel free to leave some feedback on the [Official Thread](https://discourse.pi-hole.net/t/dark-admin-dashboard/). Sadly this theme isn't for everyone, so here's a simple method to restore the origional beauty of PiHole:

1. open a terminal (or SSH if you're remoting in) ( <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>T</kbd> )
2. Type: `cd /var/www/html/admin/`
3. Type `git reset --hard`
4. Go [leave some feedback](https://discourse.pi-hole.net/t/dark-admin-dashboard/) so we know what you didn't like!

----------

## Suggestion?

Something you'd like to see? Or maybe there's an incompatability with a newer version of pi-hole? Please open an issue or make a pull request!

----------

## Credits

All credits to the original creators of these files, I am meerly an editor.
All credits to the [Pi-Hole Team](https://pi-hole.net) for their great platform.

Do follow the individual licenses of these files as supplied by the authors.

Inspired by [this thread](https://discourse.pi-hole.net/t/dark-admin-theme/647/62).

Thanks to [deathbybandaid](https://discourse.pi-hole.net/u/deathbybandaid) for the new easy install method!
